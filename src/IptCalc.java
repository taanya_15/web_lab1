import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;

public class IptCalc extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("iptCalc.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try
        {
            double sum = Double.parseDouble(request.getParameter("sum"));
            double years = Double.parseDouble(request.getParameter("years"));
            double per = Double.parseDouble(request.getParameter("proc"));

            double everyMonthPer = per / 100 / 12;
            double tmp = Math.pow(1+everyMonthPer, years*12);
            double everyMonthPay = sum*everyMonthPer*tmp / (tmp-1);
            ArrayList<Payments> monthPay = new ArrayList<>();
            double debt=sum;
            int month = (int)years*12;

            for(int i=0; i<month;i++)
            {
                Payments pay = new Payments();
                pay.setMainDebt(debt);
                pay.setPercentPart(pay.getMainDebt() * everyMonthPer);
                pay.setMainPart(everyMonthPay - pay.getPercentPart());
                debt = debt - pay.getMainPart();
                pay.setMonthPay(everyMonthPay);
                pay.RoundAll();
                monthPay.add(pay);
            }

            request.setAttribute("result", Rounding(everyMonthPay*years*12));
            request.setAttribute("result2", Rounding(everyMonthPay*years*12 - sum));
            request.setAttribute("months", monthPay);

        }
        catch (Exception ex)
        {
            request.setAttribute("error", "Не удалось посчитать ваши выплаты");
        }

        request.getRequestDispatcher("iptCalc.jsp").forward(request, response);
    }

    public class Payments
    {
        private double percentPart; //процентная часть
        private double mainPart; //основная часть
        private double mainDebt; //основной долг
        private double monthPay; //ежемесячная выплата

        public void setMonthPay(double monthPay) {
            this.monthPay = monthPay;
        }

        public double getMonthPay() {
            return monthPay;
        }

        public void setPercentPart(double percentPart)
        {
            this.percentPart = percentPart;
        }

        public void setMainPart(double mainPart)
        {
            this.mainPart = mainPart;
        }

        public void setMainDebt(double mainDebt)
        {
            this.mainDebt = mainDebt;
        }

        public double getPercentPart()
        {
            return percentPart;
        }

        public double getMainPart()
        {
            return mainPart;
        }

        public double getMainDebt()
        {
            return mainDebt;
        }

        public void RoundAll()
        {
            mainDebt = Rounding(mainDebt);
            mainPart = Rounding(mainPart);
            percentPart = Rounding(percentPart);
            monthPay = Rounding(monthPay);
        }

    }

    public double Rounding(double num)
    {
        num = Math.round(num*100);
        num = num / 100;

        return num;
    }
}
