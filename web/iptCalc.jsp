<%@ page import="java.io.PrintWriter" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%--
  Created by IntelliJ IDEA.
  User: tanya
  Date: 20.03.2020
  Time: 17:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<link href="style.css" rel="stylesheet" media="all">
<head>
    <title>Mortgage calculator</title>
</head>
<body>
    <form method="post">
       <h1>Калькулятор ипотеки</h1>
        <div>Сумма кредита </div> <input type="text" name="sum" required/>
        <br> <br>
        <div>Количество лет </div> <input type="number" name="years" required/>
        <br><br>
        <div>Процентная ставка </div> <input type="number" name="proc" required/>
        <br><br>
        <button type="submit">
            Рассчитать
        </button>
        <br><br>

    </form>

    <c:set var="result">${result}</c:set>
    <c:set var="result2">${result2}</c:set>
    <c:set var="error">${error}</c:set>
    <c:if test="${result != null}">
        <h4>Сумма полных затрат с учетом процентов: </h4>
        <h3> <c:out value="${result}"/> </h3>
        <h4>Сумма переплаты: </h4>
        <h3> <c:out value="${result2}"/> </h3>

        <table>
            <caption>Расчёт ежемесячных выплат</caption>
            <tr>
                <td>Месяц</td>
                <td>Общая выплата</td>
                <td>Основная часть</td>
                <td>Процентная часть</td>
                <td>Размер основного долга</td>
            </tr>
            <c:forEach var="month" items="${months}" varStatus="status">
                <tr>
                    <td><c:out value="${status.count}"/></td>
                    <td><c:out value="${month.monthPay}"/></td>
                    <td><c:out value="${month.mainPart}"/></td>
                    <td><c:out value="${month.percentPart}"/></td>
                    <td><c:out value="${month.mainDebt}"/></td>
                </tr>
            </c:forEach>
        </table>

    </c:if>
    <c:if test="${result == null}">
        <c:if test="${error !=null}">
          <p>  <c:out value="${error}"/> </p>
        </c:if>
    </c:if>

</body>
</html>
